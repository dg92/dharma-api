module.exports = {
  server: {
    host: '0.0.0.0',
    port: 3000
  },

  orm: {
    db: {
      client: 'postgresql',
      connection: {
        database: 'dharma_test',
        host: 'localhost',
        user: 'dev',
        password: 'dev'
      },
      pool: {
        min: 2,
        max: 10
      },
      migrations: 'knex_migrations'
    }
  },

  frontend: {
    url: 'https://pkkzelnomd.localtunnel.me'
  },

  backend: {
    url: 'https://whqgfycphr.localtunnel.me'
  },

  bitly: {
    clientId: 'a7b1b6c50ac2d36534c384f0a2f1a1b0b1379225',
    clientSecret: '424b2ac9de1226ed54209faf080122824ef1cc6b',
    username: 'denomer',
    password: 'deepak@61'
  }
};
