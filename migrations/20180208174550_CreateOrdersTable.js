function up(knex) {
  return knex.schema.createTable('orders', (t) => {
    t.uuid('id').primary();
    t.string('wallet_address');
    t.jsonb('seller_info').defaultTo('{}');
    t.jsonb('buyer_info').defaultTo('{}');
    t.jsonb('expiration').defaultTo('{}');
    t.string('taker_address');
    t.string('order_hash');
    t.jsonb('message').defaultTo('{}');
    t.timestamps();
  });
}

function down(knex) {
  return knex.schema.dropTable('orders');
}

module.exports = {up, down};