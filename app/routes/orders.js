const app = module.exports = require('express')();
const path = require('path');
const ejs = require('ejs');
const fs = require('fs');

const {frontend} = require('config');

const {
  createOrder,
  getOrderById,
  getOrders
} = require('app/actions').orders;

app.post('/', (req, res) => {
  createOrder(req.body)
    .then(({order, bitlyLink}) => {
      return res.send({order, bitlyLink});
    })
    .catch((err) => {
      res.status(400).send({err});
    })
  ;
});

app.get('/:orderId', (req, res) => {
  getOrderById(req.params.orderId).then((order) => {
    var compiled = ejs.compile(fs.readFileSync (path.join(__dirname, '../views/order.ejs'), 'utf8'));
    var html = compiled({
      title: order.message.text.slice(0, 10), 
      description: order.message.text, 
      image: order.message.png,
      frontendUrl: frontend.url+ `/orders/${order.id}`
    });
    const newHtml = html.replace(`"`, '');
    res.type("text/html");
    return res.send(newHtml);
  }).catch((e) => {
    return res.send({error: e})
  })
});

app.get('/:orderId/get-details', (req, res) => {
  getOrderById(req.params.orderId).then((order) => {
    return res.send({order : {
      wallet_address: order.wallet_address,
      seller_info: order.seller_info,
      buyer_info: order.buyer_info,
      expiration: order.expiration,
      taker: order.taker,
      order_hash: order.order_hash,
      message: {text: order.message.text}
    }})
  });
})
