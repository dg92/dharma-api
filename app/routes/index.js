const app = module.exports = require('express')();

app.get('/', (req, res) => {
  res.send({msg: 'hello'});
});

app.use('/orders', require('./orders'));

// the catch all route
app.all('*', (req, res) => {
  res.status(404).send({msg: 'not found'});
});
