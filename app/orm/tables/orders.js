export default function loadTables(orm) {
  orm.defineTable({
    name: 'orders',

    props: {
      autoId: true,
      timestamps: true
    },

    relations: {
    }
  });
}
  