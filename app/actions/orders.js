import { fileMatch } from 'file-system';

const shape = require('shape-errors');
const isUuid = require('is-uuid').v4;
const BitlyAPI = require("node-bitlyapi");
const base64Img = require('base64-img');
const imgur = require('imgur');

const fs = require('fs');
const path = require('path');
const {isString, isObject, get} = require('lodash');

const {bitly, backend} = require('config');
const {table} = require('app/orm');
const Bitly = new BitlyAPI({
	client_id: bitly.clientId,
	client_secret: bitly.clientSecret	
});

function getOrderById(id) {
  return isUuid(id) ? table('orders').find(id) : null;
}

function getOrders() {
  return table('orders').all();
}

function createOrder(data) {
  return validateOrderData(data).then((err) => {
    return err ? (
      Promise.reject(err)
    ) : (
      table('orders').insert(data).then((order) => {
        return new Promise(function(resolve, reject) {
          base64Img.img(order.message.png, path.join(__dirname, `../../public`), `${order.id}`, function(err, filepath) {
            if(err) {
              return reject(err);
            }
            imgur.uploadFile(filepath).then((json) => {
              table('orders').update(order.id, {
                message: {
                  text: order.message.text,
                  png: json.data.link.replace('https', 'http')
                }
              }).then(() => {
                return resolve();
              })
            })
          });
        }).then(() => {
          return new Promise((resolve, reject) => {
            Bitly.authenticate(bitly.username, bitly.password, function(err, access_token) {
              if(access_token) {
                Bitly.setAccessToken(access_token);
                return resolve();
              }
            });
          });
        }).then(() => {
          return new Promise((resolve, reject) => {
            Bitly.shorten({longUrl: `${backend.url}/orders/${order.id}`}, function(err, results) {
              // Do something with your new, shorter url..
              return resolve({order, bitlyLink: JSON.parse(results).data.url});
            });
          });
        });
      })
    );
  });
}

function validateOrderData(data) {
  return shape({
    wallet_address: (wallet_address) => isString(wallet_address) && wallet_address.length > 0 ? null : 'Wallet address is invalid',
    seller_info: (seller_info) => isObject(seller_info) ? null : 'Seller object is invalid',
    buyer_info: (buyer_info) => isObject(buyer_info) ? null : 'Buyer object is invalid',
    message: (message) => isObject(message) ? null : 'Message object is invalid',
    order_hash: (order_hash) => isString(order_hash) && order_hash.length > 0 ? null : 'Order hash is invalid'
  }).errors(data);
}

module.exports = {
  createOrder,
  getOrderById,
  getOrders
};
