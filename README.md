# dharma assignment backend api 

# Requirements to run
* postgresql
* node.js
* localtunnel-cli                                                                                                                                         

# Installation
* setup db as per config.js file using orm.db.connection data
* on postgresql service

# Steps to perform in terminal :
1. cd in the project dir and run npm install
2. npm run migrate latest
3. npm run server
4. lt --port 3000
5. change the backend url in config
6. open the browser and check the url given by lt
7. similarly do the same for the frontend running at port 5000 and change the frontend url in config.
